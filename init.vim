""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Pluginloader
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

call pathogen#infect()
call pathogen#helptags()


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" GUI
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" enable automatic title setting for terminals
set title
set titleold="Terminal"
set titlestring=%F
set t_8f=[38;2;%lu;%lu;%lum
set t_8b=[48;2;%lu;%lu;%lum
set cursorline

set guioptions=aegitc
set gcr=a:blinkon0
set ttyfast

let g:terminal_scrollback_buffer_size = 100000
let g:terminal_color_0 = '#3f3f3f'
let g:terminal_color_1 = '#fa3f37' 
let g:terminal_color_2 = '#a6e22e' 
let g:terminal_color_3 = '#ffdc34' 
let g:terminal_color_4 = '#669def' 
let g:terminal_color_5 = '#f92672' 
let g:terminal_color_6 = '#66efed' 
let g:terminal_color_7 = '#dcdccc' 
let g:terminal_color_8 = '#709080' 
let g:terminal_color_9 = '#fa3f37' 
let g:terminal_color_10 = '#a6e22e'
let g:terminal_color_11 = '#ffdc34'
let g:terminal_color_12 = '#669def'
let g:terminal_color_13 = '#f92672'
let g:terminal_color_14 = '#66efed'
let g:terminal_color_15 = '#ffffff'
let g:python_host_prog = '/usr/bin/python'
let g:python3_host_prog = '/usr/bin/python3'
let g:UltiSnipsExpandTrigger="<a-tab>"
let g:SuperTabDefaultCompletionType = "<c-n>"


" Remember cursor position
au BufReadPost *
\ if line("'\"") > 1 && line("'\"") <= line("$") |
\ exe "normal! g`\"" |
\ endif

" Menus I like :-)
" This must happen before the syntax system is enabled
let no_buffers_menu=1
colorscheme molokai

set mouse=a
set mousemodel=popup

set number
set relativenumber

" Make the command line one line high
set showcmd
set cmdheight=1
set laststatus=2
set linespace=3
set diffopt=filler,vertical,context:50,iwhite

"let g:airline_left_sep=' '
"let g:airline_right_sep=' '

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

set termguicolors
let g:airline_powerline_fonts = 1
let g:airline_symbols.linenr = 'Ln '
let g:airline_symbols.whitespace = '!!! '
let g:airline_theme='interru'
call unite#filters#matcher_default#use(['matcher_fuzzy'])
let g:unite_source_history_yank_enable = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Indentsettings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" go with smartindent if there is no plugin indent file.
" but don't outdent hashes
set smartindent
inoremap # X#

" Tab Settings
set smarttab
set tabstop=4


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Syntax
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" autocmd BufEnter * :syntax sync fromstart

" Better Search
set hlsearch
set incsearch

" Enable python folding
let g:pymode_folding = 1

filetype plugin indent on
syntax on

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Filesettings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:netrw_list_hide='^\.,.\(pyc\|pyo\|o\)$'

" Enable hidden buffers
set hidden

" prefer unix over windows over os9 formats
set fileformats=unix,dos,mac

" No Compatibility. That just sucks
" especially annoying on redhat/windows/osx
set nocompatible
set backspace=indent,eol,start

" customize the wildmenu
set wildmenu
set wildignore=*.dll,*.o,*.pyc,*.bak,*.exe,*.jpg,*.jpeg,*.png,*.gif,*$py.class
set wildmode=list:full

" Move Backup Files to ~/.vim/sessions
set backupdir=~/.config/shada/sessions//
set dir=~/.config/shada/sessions//
set viminfo=!,'5000,:5000,<1000,s10,h,n~/.config/shada/main.shada

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Keymappings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let mapleader = ","

nnoremap <leader>. :cd %:p:h<CR>:pwd<CR>
cabbr <expr> %% expand('%:p:h')

" quicker window switching
nnoremap <silent> <C-h> <C-w>h
nnoremap <silent> <C-j> <C-w>j
nnoremap <silent> <C-k> <C-w>k
nnoremap <silent> <C-l> <C-w>l
nnoremap <silent> <C-Left> <C-w>h
nnoremap <silent> <C-Down> <C-w>j
nnoremap <silent> <C-Up> <C-w>k
nnoremap <silent> <C-Right> <C-w>l
tnoremap <silent> <C-h> <C-\><C-n><C-w>h
tnoremap <silent> <C-j> <C-\><C-n><C-w>j
tnoremap <silent> <C-k> <C-\><C-n><C-w>k
tnoremap <silent> <C-l> <C-\><C-n><C-w>l
tnoremap <silent> <C-Left> <C-\><C-n><C-w>h
tnoremap <silent> <C-Down> <C-\><C-n><C-w>j
tnoremap <silent> <C-Up> <C-\><C-n><C-w>k
tnoremap <silent> <C-Right> <C-\><C-n><C-w>l
tnoremap <silent> <Esc> <C-\><C-n>

" arrow keys move visible lines
inoremap <silent> <Down> <C-R>=pumvisible()
      \ ? "\<lt>Down>" : "\<lt>C-O>gj"<CR>
inoremap <silent> <Up> <C-R>=pumvisible() ? "\<lt>Up>" : "\<lt>C-O>gk"<CR>
nnoremap <silent> <Down> gj
nnoremap <silent> <Up> gk
vnoremap <silent> <Down> gj
vnoremap <silent> <Up> gk
set clipboard+=unnamedplus

" And be global by default
set gdefault

" tab for brackets
nnoremap <silent> <tab> %
vnoremap <silent> <tab> %

" gundo
nnoremap <silent> <Leader>u :MundoToggle<CR>

" Disable pylint checking every save
let g:pymode_lint_on_fly = 0
let g:pymode_lint_on_write = 0
let g:pymode_lint_unmodified = 0
let g:pymode_lint_sort = ['E', 'C', 'I']
let g:pymode_rope = 0
let g:pymode_rope_regenerate_on_write = 0
let g:pymode_syntax_slow_sync = 0
let g:pymode_rope_complete_on_dot = 0
let g:pymode_rope_goto_definition_bind = '<leader>g'
let g:ycm_disable_for_files_larger_than_kb = 2048
let test#strategy = "neoterm"
let test#python#runner = 'pytest'
let g:deoplete#enable_at_startup = 1
"let g:deoplete#sources = {}
"let g:deoplete#sources._ = ['buffer', 'ultisnips', 'file', 'dictionary']
"let g:deoplete#sources.python = ['buffer', 'tag', 'ultisnips', 'file', 'dictionary']


nnoremap <silent> ,th :call neoterm#close()<cr>
nnoremap <silent> ,tl :call neoterm#clear()<cr>
nnoremap <silent> ,tc :call neoterm#kill()<cr>

" remove stupid help
map <silent> <C-b> :Explore!<CR>

autocmd CursorMovedI * if pumvisible() == 0|silent! pclose|endif
autocmd InsertLeave * if pumvisible() == 0|silent! pclose|endif
nnoremap <leader>y :<C-u>Unite -start-insert history/yank<CR>
nnoremap <leader>f :<C-u>Unite -start-insert file_rec/neovim<CR>
nnoremap <leader>F :<C-u>Unite file_rec/neovim<CR>
nnoremap <leader>t :<C-u>Unite -start-insert tab<CR>
nnoremap <leader>T :<C-u>Unite tab<CR>
nnoremap <leader>l :<C-u>Unite -start-insert buffer<CR>
nnoremap <leader>L :<C-u>Unite buffer<CR>
nnoremap <leader>m :<C-u>Unite source/vim_bookmarks<CR>
nnoremap <leader>py26 :T workon XMLAPI-py2.6.9<CR>
nnoremap <leader>py27 :T workon XMLAPI-py2.7.10<CR>
nnoremap <leader>tt :TestFile<CR>

xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)
vmap gA :s/\(\S\)\s\{2,}/\1 /

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Utils
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Remove trailing whitespace on <leader>S
nnoremap <leader>S :%s/\s\+$//<cr>:let @/=''<CR>

function UAPI_Filekeys()
  execute "normal! /\v.*UniversalRecord LocatorCode\=\"(\w{6})\".*"
  execute "v//d"
  execute "%s//\1"
  execute "sort -u"
  execute "normal! gg\<c-v>GI*\<cr>"
endfunction

function FLX_Filekeys()
  execute "normal! /\\v.*RecordLocator\\>(\\w{6}).*"
  execute "v//d"
  execute "%s//\1"
  execute "sort -u"
  execute "normal! gg\<c-v>GI*\<cr>"
endfunction

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Specific Syntax
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""" Python
autocmd FileType python setlocal expandtab
\ shiftwidth=4 tabstop=8 colorcolumn=79,100
\ formatoptions+=croq softtabstop=4 smartindent
let python_highlight_all = 1

""" Coffe
autocmd FileType coffee setlocal expandtab
\ shiftwidth=2 tabstop=4 softtabstop=2

""" XML
autocmd FileType xml setlocal expandtab
\ shiftwidth=2 tabstop=4 softtabstop=2 smartindent

""" PHP (hopefully not)
autocmd FileType php setlocal expandtab
\ shiftwidth=4 tabstop=8 softtabstop=4 smartindent

""" Lua
autocmd FileType lua setlocal expandtab shiftwidth=4 tabstop=8
\ formatoptions+=croq softtabstop=4 smartindent

""" Go
autocmd BufNewFile,BufRead *.go setlocal ft=go
autocmd FileType go setlocal expandtab
\ shiftwidth=4 tabstop=8 softtabstop=4

""" CSS
autocmd FileType css setlocal expandtab
\ shiftwidth=4 tabstop=4 softtabstop=4

""" Rst
autocmd BufNewFile,BufRead *.txt setlocal ft=rst
autocmd FileType rst setlocal expandtab
\ shiftwidth=4 tabstop=4 softtabstop=4
\ formatoptions+=nqt textwidth=74

""" C/C++
autocmd FileType c setlocal expandtab
\ tabstop=4 softtabstop=4 shiftwidth=4
autocmd FileType cpp setlocal expandtab
\ tabstop=4 softtabstop=4 shiftwidth=4

""" vim
autocmd FileType vim setlocal expandtab
\ shiftwidth=2 tabstop=8 softtabstop=2

""" javascript
autocmd FileType javascript setlocal expandtab
\ shiftwidth=2 tabstop=2 softtabstop=2
let javascript_enable_domhtmlcss=1

" Java (hopefully not)
" ----------
autocmd FileType java setlocal expandtab
\ tabstop=4 shiftwidth=4 softtabstop=4
