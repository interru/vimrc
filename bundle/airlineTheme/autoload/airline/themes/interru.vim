let g:airline#themes#interru#palette = {}

let g:airline#themes#interru#palette.accents = {
      \ 'red': [ '#66d9ef' , '' , 81 , '' , '' ],
      \ }


" Normal mode
let s:N1 = [ '#080808' , '#fa3f37' , 232 , 144 ] " mode
let s:N2 = [ '#f8f8f0' , '#080808' , 253 , 16  ] " info
let s:N3 = [ '#f8f8f0' , '#1c1c1c' , 253 , 67  ] " statusline

let g:airline#themes#interru#palette.normal = airline#themes#generate_color_map(s:N1, s:N2, s:N3)
let g:airline#themes#interru#palette.normal_modified = {
      \ 'airline_c': [ '#fa3f37' , '#212121' , 232 , 144 , '' ] ,
      \ }


" Insert mode
let s:I1 = [ '#080808' , '#a6e22e' , 232 , 81 ]
let s:I2 = [ '#f8f8f0' , '#080808' , 253 , 16 ]
let s:I3 = [ '#f8f8f0' , '#1c1c1c' , 253 , 67 ]

let g:airline#themes#interru#palette.insert = airline#themes#generate_color_map(s:I1, s:I2, s:I3)
let g:airline#themes#interru#palette.insert_modified = {
      \ 'airline_c': [ '#fa3f37' , '#212121' , 232 , 144 , '' ] ,
      \ }


" Replace mode
let g:airline#themes#interru#palette.replace = copy(g:airline#themes#interru#palette.insert)
let g:airline#themes#interru#palette.replace.airline_a = [ s:I1[0]   , '#ef5939' , s:I1[2] , 166     , ''     ]
let g:airline#themes#interru#palette.replace_modified = {
      \ 'airline_c': [ '#080808' , '#ef5939' , 232 , 166 , '' ] ,
      \ }


" Visual mode
let s:V1 = [ '#080808' , '#ffdc34' , 232 , 208 ]
let s:V2 = [ '#f8f8f0' , '#080808' , 253 , 16  ]
let s:V3 = [ '#f8f8f0' , '#1c1c1c' , 253 , 67  ]

let g:airline#themes#interru#palette.visual = airline#themes#generate_color_map(s:V1, s:V2, s:V3)
let g:airline#themes#interru#palette.visual_modified = {
      \ 'airline_c': [ '#fa3f37' , '#212121' , 232 , 144 , '' ] ,
      \ }


" Inactive
let s:IA = [ '#f8f8f0' , '#080808' , 233 , 67 , '' ]
let g:airline#themes#interru#palette.inactive = airline#themes#generate_color_map(s:IA, s:IA, s:IA)
let g:airline#themes#interru#palette.inactive_modified = {
      \ 'airline_c': [ '#fa3f37' , '#080808' , 232 , 144 , '' ] ,
      \ }
